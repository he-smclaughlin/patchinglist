#!/usr/bin/perl
#vc01-newtags.newtags
if ($ARGV[0] eq "") { print "usage: ./newtags.pl VCENTER (vc01,vc02,etc.)\n";}
#test-files/vc01-newtags.newtags
$vc = "$ARGV[0] -newtags.newtags";
$vc =~ s/ //g;
$datacenter = $ARGV[0];
$first = "yes";
#@lines = `cat /scratch/patchinglist/test-files/$vc`;
@lines = `cat /scratch/patchinglist/test-files/$ARGV[0]`;
print "Virtual Machine,hostname,patchgroup,owner,customer,application,timezone,os,datacenter,customer status\n";
foreach $line (@lines) {
	chomp $line;
	if ($line =~ /VirtualMachine/ && $first eq "yes") {
		$line =~ s/[= \/]/ /g;
		($blah,$virtualmachine)=split(" ", $line);
	#	print "$virtualmachine,";
		$virtualmachineArray{$virtualmachine} = $virtualmachine;
		$first = "no";
	}elsif ($first ne "yes" && $line =~ /^Tag/) {
		$line =~ s/\//: /g;
		if ($line =~ /Patching Group/) {
			($blah,$blah,$patchgroup)=split(": ", $line);
			$patchgroupArray{$virtualmachine}= $patchgroup;
		}		
		if ($line =~ /Internal Owner/) {
			($blah,$blah,$owner)=split(": ", $line);
			$ownerArray{$virtualmachine}= $owner;
		}		
		if ($line =~ /Customer/) {
			($blah,$blah,$customer)=split(": ", $line);
			$customerArray{$virtualmachine}= $customerArray;
		}		
		if ($line =~ /Application/) {
			($blah,$blah,$application)=split(": ", $line);
			$applicationArray{$virtualmachine}= $application;
		}		
		if ($line =~ /Time Zone/) {
			($blah,$blah,$timezone)=split(": ", $line);
			$timezoneArray{$virtualmachine}= $timezone;
		}		
		if ($line =~ /OS/) {
			($blah,$blah,$os)=split(": ", $line);
			$osArray{$virtualmachine}= $os;
		}		
#		if ($line =~ /DataCenter/) {
#			($blah,$blah,$datacenter)=split(": ", $line);
#			$datacenterArray{$virtualmachine}= $datacenter;
#		}		
		if ($line =~ /Customer Status/) {
			($blah,$blah,$customerstatus)=split(": ", $line);
			$customerstatusArray{$virtualmachine}= $customerstatus;
		}		
		if ($line =~ /Environment/) {
			($blah,$blah,$environment)=split(": ", $line);
			$environmentArray{$virtualmachine}= $environment;
		}		
		
	}elsif ($first ne "yes" && $line =~ /^Entity/) {
		($blah,$hostname)=split(": ", $line);
		$hostnameArray{$virtualmachine} = $hostname;
		$first = "yes";
	}
	
}
for $virtualmachine (keys %virtualmachineArray) {
	print "$virtualmachine,$hostnameArray{$virtualmachine},$patchgroupArray{$virtualmachine},$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$datacenterArray{$virtualmachine},$customerstatusArray{$virtualmachine}::\n";
}	
