#!/usr/bin/perl
open (FILE, "eng-exports.out");
$first = "yes";
while (<FILE>) {
	chomp $_;
	if ($_ =~ /^Name/) {
		$first = "no";
		($blah,$blah,$name)=split(" ", $_);
		print "$name,";
	}elsif ($_ =~ /^tag/ && $first ne "yes") {
		($blah,$blah,$blah,$tag)=split(" ", $_);
		($blah,$patchgroup)=split("\/", $tag);
		$patchgroup =~ s/,//g;
		print "$patchgroup,";
	}elsif ($_ =~ /^uuid/ && $first ne "yes") {
		($blah,$blah,$uuid)=split(" ", $_);
		print "$uuid,";
	}elsif ($_ =~ /^IP/ && $first ne "yes") {
		($blah,$blah,$ip)=split(" ", $_);
                print "$ip,";
	}elsif ($_ =~ /^Powerstate/ && $first ne "yes") {
		($blah,$blah,$powerstate)=split(" ", $_);
                print "$powerstate,";
	}elsif ($_ =~ /^ConfiguredOS/ && $first ne "yes") {
		($blah,$configOS)=split(":", $_);
		$configOS =~ s/ //g;
                print "$configOS,";
	}elsif ($_ =~ /^RunningOS/ && $first ne "yes") {
		($blah,$runningOS)=split(":", $_);
		$runningOS =~ s/ //g;
                print "$runningOS,";
	}elsif ($_ =~ /^PortgroupName/ && $first ne "yes") {
		($blah,$blah,$portgroupname)=split(" ", $_);
                print "$portgroupname,";
	}elsif ($_ =~ /^Notes/ && $first ne "yes") {
		($blah,$notes)=split(":", $_);
		print "$notes\n";
		$first = "yes";
	}
}


#Name          : aet-ora-vdb008
#tag           : {Patching Group/Four, Patching Group/Four, Patching Group/Three, Patching Group/Three}
#uuid          : 4222c440-f493-ace7-5f48-9cf3c2f73c5f
#IP            : 10.14.25.251
#Powerstate    : PoweredOn
#ConfiguredOS  : Red Hat Enterprise Linux 7 (64-bit)
#RunningOS     : Red Hat Enterprise Linux 7 (64-bit)
#PortgroupName : ENG-OSCONF
#Notes         : Provisioned by VMware vRA
