cd /home/a-smclaughlin/git/test/patchinglist/

mkdir files 2> /dev/null
date > files/time
echo "starting eng.notes"
mkdir temp-files 2> /dev/null
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-eng-notes.ps1  *> /home/a-smclaughlin/git/test/patchinglist/temp-files/eng.notes 

echo "starting vcs-01.notes"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-vcs01-notes.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vcs01.notes 

echo "starting vcs-02.notes"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-vcs02-notes.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vcs02.notes 

echo "starting vxvcs01.notes"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-vx1-notes.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vxvcs01.notes 

echo "starting vxvcs02.notes"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-vx1-notes.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vxvcs02.notes 
