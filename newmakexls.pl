#!/usr/bin/perl
$home = "/scratch/patchinglist";
$env = $ARGV[0];
#print "ENG $env\n";
#print "$home/files/$env.clops\n";
use Excel::Writer::XLSX;
$dcs = "eng vcs01 vcs02 vxvcs01 vxvcs02 vc01 vc02";
(@dcs) = split(" ", $dcs);
foreach $dc (@dcs) {
	chomp $dc;
	#print "cp files/$dc-linux /var/www/html/patchlist/\n"; 
}
$xlsx = "/var/www/html/patchlist/excel/test-onehappylist-$env  .xlsx";
$xlsx =~ s/ //g; 
print "xlsx $xlsx\n";
my $workbook  = Excel::Writer::XLSX->new( $xlsx );
my $worksheethe1linux  = $workbook->add_worksheet( 'he1 linux' );
my $worksheethe1windows  = $workbook->add_worksheet( 'he1 windows' );
my $worksheethe1ora  = $workbook->add_worksheet( 'he1 ora' );
my $worksheethe1dcs  = $workbook->add_worksheet( 'he1 dcs' );
my $worksheethe2linux  = $workbook->add_worksheet( 'he2 linux' );
my $worksheethe2windows  = $workbook->add_worksheet( 'he2 windows' );
my $worksheethe2ora  = $workbook->add_worksheet( 'he2 ora' );
my $worksheethe2dcs  = $workbook->add_worksheet( 'he2 dcs' );

my $worksheetenglinux  = $workbook->add_worksheet( 'eng linux' );
my $worksheetengwindows  = $workbook->add_worksheet( 'eng windows' );
my $worksheetengora  = $workbook->add_worksheet( 'eng ora' );
my $worksheetengdcs  = $workbook->add_worksheet( 'eng dcs' );
#my $worksheetclops  = $workbook->add_worksheet( 'CLOPS' );

my $heading = $workbook->add_format(
    bold  => 1,
    color => 'black',
    size  => 14,
    merge => 1,
    align => 'vcenter',
);
print "he1linux\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe1linux->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
#files/vxvcs01-linux:
@he1linux = `grep poweredon $home/new-files/*vc*01-$env-linux|sed -e "s/ , /,/g"`;
print @he1linux;

foreach $vm (@he1linux) {
	$vm =~ s/$home\/new-files\/vxvcs01-$env-linux://g;
	$vm =~ s/$home\/new-files\/vcs01-$env-linux://g;
	$vm =~ s/$home\/new-files\/vc01-$env-linux://g;
        (@fields)=split(",", $vm);
print @fields;
	chomp $fields[7];
	#print "$fields[1] $fields[7]\n";
	$worksheethe1linux->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
	$row = $row  + 1;
	$fullrow = "A $row";
	$fullrow =~ s/ //g;
}
print "he1 windows\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe1windows->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@he1windows = `grep poweredon $home/new-files/*vc*01-$env-windows|sed -e "s/ , /,/g"`;
foreach $vm (@he1windows) {
        $vm =~ s/new-files\/vxvcs01-windows://g;
        $vm =~ s/new-files\/vcs01-windows://g;
        $vm =~ s/new-files\/vc01-windows://g;
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        ##print "$fields[1] $fields[7]\n";
#print "FIELD $fields[1]\n";
        $worksheethe1windows->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}
print "he1 ora\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe1ora->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@he1ora = `grep poweredon $home/new-files/*vc*01-$env-ora|sed -e "s/ , /,/g"`;
foreach $vm (@he1ora) {
        $vm =~ s/new-files\/vxvcs01-ora://g;
        $vm =~ s/new-files\/vcs01-ora://g;
        $vm =~ s/new-files\/vc01-ora://g;
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheethe1ora->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}
print "he1 dcs\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe1dcs->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@he1dcs = `grep poweredon $home/new-files/*vc*01-$env-dcs|sed -e "s/ , /,/g"`;
foreach $vm (@he1dcs) {
        $vm =~ s/new-files\/vxvcs01-dcs://g;
        $vm =~ s/new-files\/vcs01-dcs://g;
        $vm =~ s/new-files\/vc01-dcs://g;
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheethe1dcs->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}

print "he2 linux\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe2linux->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@he2linux = `grep poweredon $home/new-files/*vc*02-$env-linux|sed -e "s/ , /,/g"`;
foreach $vm (@he2linux) {
        $vm =~ s/new-files\/vxvcs02-linux://g;
        $vm =~ s/new-files\/vcs02-linux://g;
        $vm =~ s/new-files\/vc02-linux://g;
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheethe2linux->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}
print "he2 windows\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe2windows->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@he2windows = `grep poweredon $home/new-files/*vc*02-$env-windows|sed -e "s/ , /,/g"`;
foreach $vm (@he2windows) {
        $vm =~ s/new-files\/vxvcs02-windows://g;
        $vm =~ s/new-files\/vcs02-windows://g;
        $vm =~ s/new-files\/vc02-windows://g;
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheethe2windows->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}

print "he2 ora\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe2ora->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@he2ora = `grep poweredon $home/new-files/*vc*02-$env-ora|sed -e "s/ , /,/g"`;
foreach $vm (@he2ora) {
        $vm =~ s/new-files\/vxvcs02-ora://g;
        $vm =~ s/new-files\/vcs02-ora://g;
        $vm =~ s/new-files\/vcs01-ora://g;
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheethe2ora->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}

print "he2 dcs\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','UUID','clopsENV' );
$worksheethe2dcs->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@he2dcs = `grep poweredon $home/new-files/*vc*02-$env-dcs|sed -e "s/ , /,/g"`;
foreach $vm (@he2dcs) {
        $vm =~ s/new-files\/vxvcs02-dcs://g;
        $vm =~ s/new-files\/vcs02-dcs://g;
        $vm =~ s/new-files\/vc02-dcs://g;
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheethe2dcs->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}



print "eng linux\n"; 
my @headings = ('vcenter', 'hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheetenglinux->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
@englinux = `grep poweredon $home/new-files/eng-$env-linux|sed -e "s/ , /,/g"`;
#print "grep poweredon $home/new-files/eng-$env-linux\n";
foreach $vm (@englinux) {
#        $vm =~ s/new-files\/vxvcs02-linux://g;
#        $vm =~ s/new-files\/vcs02-linux://g;
#        ($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheetenglinux->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}

print "eng windows\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheetengwindows->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@engwindows = `grep poweredon $home/new-files/eng-$env-windows|sed -e "s/ , /,/g"`;
foreach $vm (@engwindows) {
#        $vm =~ s/new-files\/vxvcs02-windows://g";
#        $vm =~ s/new-files\/vcs02-windows://g";
#	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheetengwindows->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}

print "eng ora\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheetengora->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@engora = `grep poweredon $home/new-files/eng-$env-ora|sed -e "s/ , /,/g"`;
foreach $vm (@engora) {
#        $vm =~ s/new-files\/vxvcs02-ora://g";
#        $vm =~ s/new-files\/vcs02-ora://g";
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheetengora->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}

print "eng dcs\n"; 
my @headings = ( 'vcenter','hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheetengdcs->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
#print "$fullrow\n";
@engdcs = `grep poweredon $home/new-files/eng-$env-dcs|sed -e "s/ , /,/g"`;
foreach $vm (@engora) {
#        $vm =~ s/new-files\/vxvcs02-ora://g";
#        $vm =~ s/new-files\/vcs02-ora://g";
	($blah,$vm)=split(":", $vm);
        (@fields)=split(",", $vm);
        chomp $fields[7];
        #print "$fields[1] $fields[7]\n";
        $worksheetengdcs->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A $row";
        $fullrow =~ s/ //g;
}

#my @headings = ( 'vcenter','hostname','CLOPS');
#$worksheetclops->write_row( 'A1', \@headings, $heading );
#$row = 2;
#$fullrow = "A$row";
##print "$fullrow\n";
#print "DOING CLOPS\n";
#print "FILE $home/new-files/$env.clops\n";
#@clops = `cat $home/new-files/$env.clops`;
#print "DID CAT\n";
#foreach $vm (@clops) {
#        (@fields)=split(",", $vm);
#	#print "CLOP FIELDS $fields[0] $fields[0] $fields[1]\n";
#        $worksheetclops->write_row( $fullrow, \@fields, $heading );
#       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
#        $row = $row  + 1;
#        $fullrow = "A $row";
#        $fullrow =~ s/ //g;
#}
