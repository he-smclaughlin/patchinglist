#!/usr/bin/perl
&load_vcenter_arrays;
$first = "yes";
$dcs = "eng vc01 vc02 vcs01 vcs02 vxvcs01 vxvcs02";
(@dcs)=split(" ", $dcs);
&fill_powerstate_array;
foreach $dc (@dcs) {
	chomp $dc;
	@lines = `cat /scratch/patchinglist/test-files/$dc.tags.tmp|grep -iv copy.shadow|grep -vi poweredoff`;
	foreach $line (@lines) {
		chomp $line;
		if ($line =~ /virtualmachine-vm-/ && $first eq "yes") {
			$line =~ s/[= \/]/ /g;
			($blah,$virtualmachine1,$virtualmachine2)=split(" ", $line);
			if ($virtualmachine1 =~ /virtualmachine-vm-/) { $virtualmachine = $virtualmachine1;}
			if ($virtualmachine2 =~ /virtualmachine-vm-/) { $virtualmachine = $virtualmachine2;}
			if ($virtualmachine eq "virtualmachine") { print "BROKE $line\n";}
			$hostname =~ tr/[A-Z]/[a-z]/;
			$virtualmachine =~ tr/[A-Z]/[a-z]/;
			$dcArray{"$virtualmachine$hostname"} = $dc;
			$virtualmachineArray{"$virtualmachine$hostname"} = $virtualmachine;
			$first = "no";
		}
		if ($line =~ /^entity/ && $first eq "no") {
			($blah,$blah,$hostname)=split(" ", $line);
			#print "$hostname\n";
			$virthost = "$virtualmachine$hostname";
			$hostnameArray{$virthost} = $hostname;
			$uniqArray{$virthost} = $dc;
			$first = "yes";
		}
		if ($line =~ /^tag/) {
			($blah,$blah,$tag)=split(" ", $line);
	         	if ($line =~ /patching group/) {
	                        ($blah,$blah,$patchgroup)=split(": ", $line);
       				$patchgroupArray{"$virtualmachine$hostname"}= $patchgroup;
                	}
			if ($line =~ /internal owner/) {
				#print "found internal owner\n";
	                        ($blah,$owner)=split("\/", $line);
	                        $ownerArray{"$virtualmachine$hostname"}= $owner;
	                }
	                if ($line =~ /customer/) {
#print "found cust\n";
	                        ($blah,$customer)=split("\/", $line);
	                        $customerArray{"$virtualmachine$hostname"}= $customerArray;
	                }
	                if ($line =~ /application/) {
	#print "found appl\n";
	                        ($blah,$application)=split("\/", $line);
	                        $applicationArray{"$virtualmachine$hostname"}= $application;
	                }
	                if ($line =~ /time zone/) {
#	print "found timezo\n";
	                        ($blah,$timezone)=split("\/", $line);
				$timezone = join " ", map {ucfirst} split " ", $timezone;
	                        $timezoneArray{"$virtualmachine$hostname"}= $timezone;
	                }
	                if ($line =~ /os/) {
	#print "found os\n";
	                        ($blah,$os)=split("\/", $line);
	                        $osArray{"$virtualmachine$hostname"}= $os;
	                }
	                if ($line =~ /customer status/) {
	#print "found cust stat\n";
	                        ($blah,$customerstatus)=split("\/", $line);
	                        $customerstatusArray{"$virtualmachine$hostname"}= $customerstatus;
	                }
	                if ($line =~ /environment/) {
	#print "found env\n";
	                        ($blah,$environment)=split("\/", $line);
	                        $environmentArray{"$virtualmachine$hostname"}= $environment;
	                }
		}
	}
	#print "Vcenter,Virtual Machine,hostname,patchgroup,owner,customer,application,timezone,os,datacenter,customer status\n";
for $virtualmachine (keys %virtualmachineArray) {
	chomp $virtualmachine;
	#print "$vcenterArray{"$virtualmachine$hostname"},$virtualmachine,$hostnameArray{"$virtualmachine$hostname"},$patchgroupArray{"$virtualmachine$hostname"},$ownerArray{"$virtualmachine$hostname"},$customerArray{"$virtualmachine$hostname"},$applicationArray{"$virtualmachine$hostname"},$timezoneArray{"$virtualmachine$hostname"},$osArray{"$virtualmachine$hostname"},$datacenterArray{"$virtualmachine$hostname"},$customerstatusArray{"$virtualmachine$hostname"}\n";
	
}
}
open (UPDATEDTAGS, "missingtags-updated.csv.lower");
while (<UPDATEDTAGS>) {
        chomp $_;
        $_ =~ tr/[A-Z]/[a-z]/;
        #Virtual Machine,hostname,patchgroup,owner,customer,application,timezone,os,datacenter,customer status
        ($virtualmachine,$hostname,$patchgroup,$owner,$customer,$application,$timezone,$os,$datacenter,$customerstatus)=split(",", $_);
	$timezone = join " ", map {ucfirst} split " ", $timezone;
	$virthost = "$virtualmachine$hostname";
        $customer =~ tr/[a-z]/[A-Z]/;
        if ($patchgroupArray{$virtualmachine} ne $patchgroup) {
        #       print "$hostname is WRONG $patchgroupArray{$virtualmachine}:$patchgroup\n";
        }else{
#               print "$hostname is CORRECT $patchgroupArray{$virtualmachine}:$patchgroup\n";
        }

        if ($ownerArray{$virtualmachine} ne $owner && $owner ne "") {
#               print "$hostname is WRONG $ownerArray{$virtualmachine}:$owner\n";
        }else{
#               print "$hostname is CORRECT $ownerArray{$virtualmachine}:$owner\n";
        }
        if ($timezoneArray{$virthost} ne $timezone && $timezone ne "") {
	        $virthost = "$virtualmachine$hostname";
              	$dc = $uniqArray{$virthost};
#GOOD#		print "$hostname $timezone $dc $timezoneArray{$virthost} $vcenterArray{$dc} $timezoneArray{$virthost}\n";
		#print "$hostname $timezone - DC $dc DC ~ $virtualmachine$hostname\tvcenterArray{$dc}\t$hostname\t$timezone\t$virthost\t$uniqArray{$virthost}\n";
#            	`echo "$hostname $timezone $dc  new-files/$vcenterArray{$dc}  /dev/null `;
           # 	`echo "$hostname $timezone $dc" >> new-files/$vcenterArray{$dc} 2> /dev/null `;
	}


        if ($patchgroup{$virthost} ne $patchgroup && $patchgroup ne "") {
                $virthost = "$virtualmachine$hostname";
                $dc = $uniqArray{$virthost};
     	        print "GOOD $hostname $patchgroup $dc $patchgroupArray{$virthost} $vcenterArray{$dc} $virthost\n";
                #print "$hostname $timezone - DC $dc DC ~ $virtualmachine$hostname\tvcenterArray{$dc}\t$hostname\t$timezone\t$virthost\t$uniqArray{$virthost}\n";
#               `echo "$hostname $timezone $dc  new-files/$vcenterArray{$dc}  /dev/null `;
           #    `echo "$hostname $timezone $dc" >> new-files/$vcenterArray{$dc} 2> /dev/null `;
        }else{




        #}else{
               print "$hostname is CORRECT $timezoneArray{$virtualmachine}:$timezone\n";
        }
}


sub load_vcenter_arrays {
        $vcenterArray{"eng"} = "he1-engvc-01.headquarters.healthedge.com";
        $vcenterArray{"vcs01"} = "he1-vcs-01.headquarters.healthedge.com";
        $vcenterArray{"vcs02"} = "he2-vcs-01.headquarters.healthedge.com";
        $vcenterArray{"vc01"} = "he1-vc-01.healthedge.biz";
        $vcenterArray{"vc02"} = "he2-vc-01.healthedge.biz";
        $vcenterArray{"vxvcs01"} = "he1-vxvcs-01.healthedge.biz";
        $vcenterArray{"vxvcs02"} = "he2-vxvcs-01.healthedge.biz";
}
sub fill_powerstate_array {
	@vms = `cat test-files/*list*|grep -vi poweredoff`;
	foreach $vm (@vms) {
		chomp $vm;
		($hostname,$ip,$power,$os1,$os2,$network,$blah,$blah,$uuid,$virtualmachine)=split(" , ", $vm);
		$virthost = "$virtualmachine$hostname";
		$powerstateArray{$virthost} = $power;
	}
}



