#!/usr/bin/perl
$env = $ARGV[0];
use Excel::Writer::XLSX;
$dcs = "eng vcs01 vcs02 vxvcs01 vxvcs02";
(@dcs) = split(" ", $dcs);
foreach $dc (@dcs) {
	chomp $dc;
	print "cp files/$dc-linux /var/www/html/patchlist/\n"; 
}
$xlsx = "/var/www/html/patchlist/excel/ $env .xlsx";
$xlsx =~ s/ //g; 
my $workbook   = Excel::Writer::XLSX->new( $xlsx );
my $worksheet  = $workbook->add_worksheet( 'linux' );
my $worksheet2  = $workbook->add_worksheet( 'windows' );
my $worksheet3  = $workbook->add_worksheet( 'ora' );
my $worksheet4  = $workbook->add_worksheet( 'dcs' );
#my $worksheet5  = $workbook->add_worksheet( 'missing' );
#my $worksheet6  = $workbook->add_worksheet( 'full' );

my $heading = $workbook->add_format(
    bold  => 1,
    color => 'black',
    size  => 14,
    merge => 1,
    align => 'vcenter',
);
 
my @headings = ( 'hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheet->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
print "$fullrow\n";
@vms1 = `grep poweredon files/$env-linux|sed -e "s/ , /,/g"`;
foreach $vm (@vms1) {
        (@fields)=split(",", $vm);
	chomp $fields[7];
	print "$fields[1] $fields[7]\n";
	$worksheet->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
	$row = $row  + 1;
	$fullrow = "A $row";
	$fullrow =~ s/ //g;
}

my @headings = ( 'hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheet2->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
print "$fullrow\n";
@vms2 = `grep poweredon files/$env-windows|sed -e "s/ , /,/g"`;
foreach $vm (@vms2) {
        (@fields)=split(",", $vm);
        $worksheet2->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A$row";
}

my @headings = ( 'hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheet3->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
print "$fullrow\n";
@vms3 = `grep poweredon files/$env-ora|sed -e "s/ , /,/g"`;
foreach $vm (@vms3) {
        (@fields)=split(",", $vm);
        $worksheet3->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A$row";
}

my @headings = ( 'hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
$worksheet4->write_row( 'A1', \@headings, $heading );
$row = 2;
$fullrow = "A$row";
print "$fullrow\n";
@vms4 = `grep poweredon files/$env-dcs|sed -e "s/ , /,/g"`;
foreach $vm (@vms4) {
        (@fields)=split(",", $vm);
        $worksheet4->write_row( $fullrow, \@fields, $heading );
       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
        $row = $row  + 1;
        $fullrow = "A$row";
}

#my @headings = ( 'hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
#$worksheet5->write_row( 'A1', \@headings, $heading );
#$row = 2;
#$fullrow = "A$row";
#print "$fullrow\n";
#@vms5 = `grep poweredon files/$env-missing|sed -e "s/ , /,/g"`;
#foreach $vm (@vms5) {
#        (@fields)=split(",", $vm);
#        $worksheet5->write_row( $fullrow, \@fields, $heading );
#       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
#        $row = $row  + 1;
#        $fullrow = "A$row";
#}

#my @headings = ( 'hostname','ip','power','os1','os2','network','Patch group','clopsENV','realOS', 'kernel','uptime','sssd','updates','dupes' );
#$worksheet->write_row( 'A1', \@headings, $heading );
#$row = 2;
#$fullrow = "A$row";
#print "$fullrow\n";
#@vms6 = `grep poweredon files/$env-full|sed -e "s/ , /,/g"`;
#foreach $vm (@vms6) {
#        (@fields)=split(",", $vm);
#        print "$fields[6]\n";
#        $worksheet->write_row( $fullrow, \@fields, $heading );
#       # $worksheet->write_row($hostname,$ip,$power,$os1,$os2,$network);
#        $row = $row  + 1;
#        $fullrow = "A $row";
#        $fullrow =~ s/ //g;
#}
