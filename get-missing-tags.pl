#!/usr/bin/perl
#vc01-newtags.newtags
#if ($ARGV[0] eq "") { print "usage: ./get-missing-tags.pl VCENTER (vc01,vc02,etc.)\n";exit;}
##test-files/vc01-newtags.newtags
#$vc = "$ARGV[0] -newtags.newtags";
#$vc =~ s/ //g;
#$datacenter = $ARGV[0];
$first = "yes";
$dcs = "eng vc01 vc02 vcs01 vcs02 vxvcs01 vxvcs02";
(@dcs)=split(" ", $dcs);
#@lines = `cat /scratch/patchinglist/test-files/$vc`;
foreach $dc (@dcs) {
	chomp $dc;
	@lines = `cat /scratch/patchinglist/test-files/$dc.tags.tmp|grep -iv copy.shadow|grep -iv poweredoff`;
#print "DC $dc DC\n";
	foreach $line (@lines) {
		chomp $line;
		if ($line =~ /VirtualMachine-vm-/ && $first eq "yes") {
			$line =~ s/[= \/]/ /g;
			($blah,$virtualmachine)=split(" ", $line);
			#print "$virtualmachine,";
			$vcenterArray{$virtualmachine} = $dc;
			$virtualmachineArray{$virtualmachine} = $virtualmachine;
			$first = "no";
		}
		if ($line =~ /^Entity/ && $first eq "no") {
			($blah,$blah,$hostname)=split(" ", $line);
			#print "$hostname\n";
			$hostnameArray{$virtualmachine} = $hostname;
			$first = "yes";
		}
		if ($line =~ /^Tag/) {
			($blah,$blah,$tag)=split(" ", $line);
			#print "$tag,";
	         	if ($line =~ /Patching Group/) {
#print "found patchgroup\n";
	                        ($blah,$blah,$patchgroup)=split(": ", $line);
       				$patchgroupArray{$virtualmachine}= $patchgroup;
                	}
			if ($line =~ /Internal Owner/) {
				#print "found internal owner\n";
	                        ($blah,$owner)=split("\/", $line);
	                        $ownerArray{$virtualmachine}= $owner;
	                }
	                if ($line =~ /Customer/ && $line !~ /Customer Status/) {
#print "found cust\n";
	                        ($blah,$customer)=split("\/", $line);
	                        $customerArray{$virtualmachine}= $customerArray;
	                }
	                if ($line =~ /Application/) {
	#print "found appl\n";
	                        ($blah,$application)=split("\/", $line);
	                        $applicationArray{$virtualmachine}= $application;
	                }
	                if ($line =~ /Time Zone/) {
	#print "found timezo\n";
	                        ($blah,$timezone)=split("\/", $line);
	                        $timezoneArray{$virtualmachine}= $timezone;
	                }
	                if ($line =~ /OS/) {
	#print "found os\n";
	                        ($blah,$os)=split("\/", $line);
	                        $osArray{$virtualmachine}= $os;
	                }
	                if ($line =~ /Customer Status/) {
	#print "found cust stat\n";
	                        ($blah,$customerstatus)=split("\/", $line);
	                        $customerstatusArray{$virtualmachine}= $customerstatus;
	                }
	                if ($line =~ /Environment/) {
	#print "found env\n";
	                        ($blah,$environment)=split("\/", $line);
	                        $environmentArray{$virtualmachine}= $environment;
	                }
		}
	}
	print "Vcenter,Virtual Machine,hostname,patchgroup,owner,customer,application,timezone,os,datacenter,customer status\n";
for $virtualmachine (keys %virtualmachineArray) {
	chomp $virtualmachine;
	print "$vcenterArray{$virtualmachine},$virtualmachine,$hostnameArray{$virtualmachine},$patchgroupArray{$virtualmachine},$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$datacenterArray{$virtualmachine},$customerstatusArray{$virtualmachine}\n";
	
}
}
