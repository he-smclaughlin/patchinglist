export DIR="/home/a-smclaughlin/patchinglist";
cd $DIR
mkdir files 2> /dev/null
date > files/time
echo "starting eng.list"
mkdir temp-files 2> /dev/null

#/usr/bin/pwsh $DIR/get-patchinglist-eng.ps1  *> $DIR/temp-files/eng.list & 
#/usr/bin/pwsh $DIR/get-patchinglist-vcs1.ps1 *> $DIR/temp-files/vcs01.list & 
#/usr/bin/pwsh $DIR/get-patchinglist-vcs2.ps1 *> $DIR/temp-files/vcs02.list  &
#/usr/bin/pwsh $DIR/get-patchinglist-vx1.ps1 *> $DIR/temp-files/vxvcs01.list  &
#/usr/bin/pwsh $DIR/get-patchinglist-vx2.ps1 *> $DIR/temp-files/vxvcs02.list  

#echo "starting eng.tags"
#/usr/bin/pwsh $DIR/get-eng-tags.ps1 *> $DIR/temp-files/eng.tags &
#echo "starting vcs01.tags"
#/usr/bin/pwsh $DIR/get-he1leg-tags.ps1 *> $DIR/temp-files/vcs01.tags &
#echo "starting vxvcs01.tags"
#/usr/bin/pwsh $DIR/get-he1vx-tags.ps1 *> $DIR/temp-files/vxvcs01.tags &
#echo "starting vcs02.tags"
#/usr/bin/pwsh $DIR/get-he2leg-tags.ps1 *> $DIR/temp-files/vcs02.tags &
#echo "starting vxvcs02.tags"
#/usr/bin/pwsh $DIR/get-he2vx-tags.ps1 *> $DIR/temp-files/vxvcs02.tags
#
#/usr/bin/pwsh $DIR/get-eng-notes.ps1  *> $DIR/temp-files/eng.notes &
#echo "starting vcs-01.notes"
#/usr/bin/pwsh $DIR/get-vcs01-notes.ps1 *> $DIR/temp-files/vcs01.notes &
#echo "starting vcs-02.notes"
#/usr/bin/pwsh $DIR/get-vcs02-notes.ps1 *> $DIR/temp-files/vcs02.notes &
#echo "starting vxvcs01.notes"
#/usr/bin/pwsh $DIR/get-vx1-notes.ps1 *> $DIR/temp-files/vxvcs01.notes &
#echo "starting vxvcs02.notes"
#/usr/bin/pwsh $DIR/get-vx2-notes.ps1 *> $DIR/temp-files/vxvcs02.notes 

for a in zero one two three four; do $DIR/process-files.pl eng $a; $DIR/process-files.pl vcs01 $a; $DIR/process-files.pl vcs02 $a; $DIR/process-files.pl vxvcs01 $a; $DIR/process-files.pl vxvcs02 $a ; done 
#$DIR/process-files.pl eng $a;
#$DIR/process-files.pl vcs01 $a;
#$DIR/process-files.pl vcs02 $a;
#$DIR/process-files.pl vxvcs01 $a;
#$DIR/process-files.pl vxvcs02 $a
#; done
cat $DIR/files/*zero*full|grep -i pdv|awk -F, '{print $1","$8",,,,,,,,,,,,"}' |sort|uniq > $DIR/html/zero.clops
cat $DIR/files/*one*full|grep -i pdv|awk -F, '{print $1","$8",,,,,,,,,,,,"}' |sort|uniq > $DIR/html/one.clops
cat $DIR/files/*two*full|grep -i pdv|awk -F, '{print $1","$8",,,,,,,,,,,,"}' |sort|uniq > $DIR/html/two.clops
cat $DIR/files/*three*full|grep -i pdv|awk -F, '{print $1","$8",,,,,,,,,,,,"}' |sort|uniq > $DIR/html/three.clops
cat $DIR/files/*four*full|grep -i pdv|awk -F, '{print $1","$8",,,,,,,,,,,,"}' |sort|uniq > $DIR/html/four.clops

cat /home/a-smclaughlin/patchinglist/files/*excluded*full|grep -i pdv|awk -F, '{print $1","$8}' |sort|uniq > files/excluded.clops
date >> files/time
cat files/time | mail -s "files created" smclaughlin@healthedge.com

#./makexls.pl eng
#./makexls.pl vcs01
#./makexls.pl vcs02
#./makexls.pl vxvcs01
#./makexls.pl vxvcs02
