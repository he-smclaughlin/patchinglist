$username = "a-smclaughlin"
$cat = "Customer"
$encrypted = Get-Content /scratch/tagging/encrypted_password.txt | ConvertTo-SecureString
$credential = New-Object System.Management.Automation.PsCredential($username, $encrypted)
Connect-VIServer -Server $args -Protocol https -Credential $credential
New-TagCategory -Name $cat
$FileContent = Get-content /scratch/patchinglist/customertags
foreach($tag in $FileContent) {
	New-Tag -Name $tag -Category $cat
}
