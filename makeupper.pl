#!/usr/bin/perl
$file = $ARGV[0];
if ($ARGV[0] eq "") {
        print "give a filename, exiting"; exit;
}
&makelower;

sub makelower {
	open (FILE, "$file");
	if ($ARGV[1] ne "-f") {
	        while (<FILE>) {
	                $_ =~ tr/[a-z]/[A-Z]/;
	                print $_;
	        }
	}
	if ($ARGV[1] eq "-f") {
	        $seconds = `date +%s`;
	        chomp $seconds;
	        open (OUT, "> $ARGV[0].$seconds");
	        while (<FILE>) {
	                $_ =~ tr/[a-z]/[A-z]/;
	                print OUT $_;
	        }
	        `mv -f $ARGV[0].$seconds $ARGV[0]`;
	}

}
