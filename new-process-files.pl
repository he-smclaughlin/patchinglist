#!/usr/bin/perl
# get info from lists files
# list of vcenters
#`rm -f new-files/*`;
$vcenters = "vc01 vc02 vcs01 vcs02 vxvcs01 vxvcs02 eng";
(@vcenters)=split(" ", $vcenters);
foreach $vcenter (@vcenters) {
	chomp $vcenter;
	`rm -f new-files/$vcenter-*.TMP`;
#	&load_notes;
	&load_excludes;
	&load_tags;
	&load_lists;
}

sub load_tags {
`/usr/local/bin/makelower.pl /scratch/patchinglist/test-files/$vcenter.tags.tmp -f`;
open (TAGS, "/scratch/patchinglist/test-files/$vcenter.tags.tmp");
while (<TAGS>) {
        chomp $_;
	$_ =~ tr/[A-Z]/[a-z]/;
	if ($_ =~ /virtualmachine/) {
		$_ =~ tr/[A-Z]/[a-z]/;
		$_ =~ s/[=\/]/ /g;
		($blah,$virtualmachine1,$virtualmachine2)=split(" ", $_);
		if ($virtualmachine1 eq "virtualmachine") { $virtualmachine = $virtualmachine2;}	
		if ($virtualmachine1 ne "virtualmachine") { $virtualmachine = $virtualmachine1;}	
		##SPM##print "TAGV $virtualmachine ";
	}
	if ($_ =~ /^tag/ && $_ =~ /patching group/) {
##SPM##print "FULLTAG $_\n";
		$_ =~ tr/[A-Z]/[a-z]/;
		($blah,$tag)=split(": ", $_);
		($blah,$tag)=split("\/", $_);
		$virtualmachine =~ tr/[A-Z]/[a-z]/;
		$tag =~ tr/[A-Z]/[a-z]/;
		$patchgroupArray{$virtualmachine} = $tag;	
		##SPM##print "TAGT $tag ";
#print "$tag $patchgroupArray{$virtualmachine} $virtualmachine\n";
	}
	

                        if ($_ =~ /^tag/ && $_ =~ /internal owner/) {
                                ($blah,$owner)=split("\/", $_);
#                                print "TAG found internal owner TAG $owner\n";
                                $ownerArray{$virtualmachine}= $owner;
                        }

                        if ($_ =~ /^tag/ && $_ =~ /customer/ && $_ !~ /customer status/) {
                                ($blah,$customer)=split("\/", $_);
                                ##SPM##print "TAG found CUSTOMER TAG $customer\n";
                                $customerArray{$virtualmachine}= $customer;
                        }
			if ($_ =~ /^tag/ && $_ =~ /application/) {
                                ($blah,$application)=split("\/", $_);
                                #print "TAG found application TAG $application\n";
                                $applicationArray{$virtualmachine}= $application;
                        }

                        if ($_ =~ /^tag/ && $_ =~ /time zone/) {
                                ($blah,$timezone)=split("\/", $_);
                                #print "TAG found time zone TAG $timezone\n";
                                $timezoneArray{$virtualmachine}= $timezone;
                        }
                        if ($_ =~ /^tag/ && $_ =~ /os/) {
                                ($blah,$os)=split("\/", $_);
                                #print "TAG found OS TAG $os\n";
                                $osArray{$virtualmachine}= $os;
                        }
                        if ($_ =~ /^tag/ && $_ =~ /customer status/) {
                                ($blah,$customerstatus)=split("\/", $_);
                                #print "TAG found customer status $customerstatus\n";
                                $customerstatusArray{$virtualmachine}= $customerstatus;
                        }
                        if ($_ =~ /^tag/ && $_ =~ /environment/) {
                                ($blah,$environment)=split("\/", $_);
                                #print "TAG found ENVIRONMENT $$environment\n";
                                $environmentArray{$virtualmachine}= $environment;
                        }





	if ($_ =~ /^entity/) {
		$_ =~ tr/[A-Z]/[a-z]/;
		($blah,$blah,$hostname)=split(" ", $_);
		##SPM##print "TAGSH $hostname\n";
	}
}
}

#sub load_notes {
	#open (NOTES, "test-files/$vcenter.notes");	
#	open (NOTES, "/scratch/patchinglist/test-files/$vcenter.notes");	
#	while (<NOTES>) {
#		chomp $_;
#		($hostname,$ip,$notes,$power,$uuid)=split(" , ", $_);
#		$notesArray{$uuid} = $notes;
#print "UUID IN NOTES H $hostname U $uuid N $notes\n";
#	} 
#}

sub load_excludes {
	`/usr/local/bin/makelower.pl /scratch/patchinglist/excludes-ids -f`;
	@excludes = `cat /scratch/patchinglist/excludes-ids 2> /dev/null`;
	foreach $excludeID (@excludes) {
		chomp $excludeID;
		$excludeArray{$excludeID} = T;
	}
	`/usr/local/bin/makelower.pl /scratch/patchinglist/excludes -f`;
	@excludes = `cat /scratch/patchinglist/excludes 2> /dev/null`;
	foreach $exclude (@excludes) {
		chomp $exclude;
		$excludeArray{$exclude} = T;
	}
	`/usr/local/bin/makelower.pl /scratch/patchinglist/excludes-OS -f`;
	@excludesOS = `cat /scratch/patchinglist/excludes-OS 2> /dev/null`;
	foreach $excludeOS (@excludesOS) {
		chomp $excludeOS;
		$excludeOS =~ s/[\(\)]//g;
		$excludeOSArray{$excludeOS} = T;
		##SPM##print "EXCLUDE OS $excludeOS\n";
	}
}





sub load_lists {
#	`/usr/local/bin/makelower.pl /scratch/patchinglist/test-files/$vcenter.list.tmp -f `;
	@list = `grep -i poweredon /scratch/patchinglist/test-files/$vcenter.list.tmp |grep -v ^rp|sed -e "s/ , /,/g"`;
	foreach $list (@list) {
		chomp $list;
#		$list =~ tr/[A-Z]/[a-z]/;
        	($hostname,$ip,$power,$os1,$os2,$network,$blah,$notes,$uuid,$virtualmachine)=split(",", $list);
##SPM##print "VMA :$virtualmachine:\n";
		$hostname =~ tr/[A-Z]/[a-z]/; $ip =~ tr/[A-Z]/[a-z]/; $power =~ tr/[A-Z]/[a-z]/; $os1 =~ tr/[A-Z]/[a-z]/; $os2 =~ tr/[A-Z]/[a-z]/; $network =~ tr/[A-Z]/[a-z]/; $uuid =~ tr/[A-Z]/[a-z]/; $virtualmachine =~ tr/[A-Z]/[a-z]/;
		$os1 =~ s/[\(\)]//g;
		$os2 =~ s/[\(\)]//g;
		if ($ip =~ / / && $ip =~ /^172/) {
			($ip)=split(" ", $ip);
		#	print "$ip\n";
		}elsif ($ip =~ / / && $ip =~ / fe80/) {
			($ip)=split(" ", $ip);
			#print "$ip\n";
		}elsif ($ip =~ / / && $ip =~ /^2002/) {
			($blah,$blah,$ip)=split(" ", $ip);
			#print "$ip\n";
		}elsif ($ip =~ / / && $ip =~ /^10/ && $ip =~ / 172/) {
			($ip)=split(" ", $ip);
		}
		$virtualmachine =~ tr/[A-Z]/[a-z]/;
                ##SPM##print "TAGS $ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine}\n";


		if ($excludeArray{$uuid} eq "" && $excludeArray{$hostname} eq "" ) { #&& $excludeArray{$os1} ne "" && $excludeArray{$os2} ne "") {
##SPM##print "EXCLUDEARRAY $excludeArray{$uuid} UUID $uuid EXLH $excludeArray{$hostname} H $hostname\n";
			if ($excludeOSArray{$os1} ne "" || $excludeOSArray{$os2} ne "") { print "EXCLUDEOS FOUND $hostname $ip $os1 $os2 \n";
				open (LIST, ">> new-files/$vcenter-excludeded-linux.TMP");
				#&printLine;
                                #print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notesArray{$virtualmachine}\n";
				close LIST;
			}

#			print "$virtualmachine,$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$note\n";
			if ($os1 =~ /linux 6/ || $os1 =~ /centos 6/) {
				open (LIST, ">> new-files/$vcenter-$patchgroupArray{$virtualmachine}-cent6.TMP");
				&printLine;
                                #print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notesArray{$virtualmachine}\n";
			}
			if (($os1 =~ /linux/ || $os1 =~ /centos/) && $patchgroupArray{$virtualmachine} ne "" && $excludeOSArray{$os1} eq "" && $excludeOSArray{$os2} eq "") {
##SPM##print "FOUND LINUX $vcenter $patchgroupArray{$virtualmachine} $virtualmachine\n";
				open (LIST, ">> new-files/$vcenter-$patchgroupArray{$virtualmachine}-linux.TMP");		
				&printLine;
				#print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notesArray{$virtualmachine}\n";
				#print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$virtualmachineArray{$virtualmachine},$uuid,$note\n";
				open (CLOPS, ">> new-files/clops.$patchgroupArray{$virtualmachine}.TMP");
				print CLOPS "$notes\n";
				close CLOPS;
				open (FULL, ">> new-files/$vcenter-$patchgroupArray{$virtualmachine}-full.TMP");		
				&printFull;
				#print FULL "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notesArray{$virtualmachine}\n";
				close FULL;
				close LIST;
			}
			if (($os1 =~ /windows/ || $os1 =~ /Windows/) && $patchgroupArray{$virtualmachine} ne "" && $excludeOSArray{$os1} eq "" && $excludeOSArray{$os2} eq "") {
##SPM##print "FOUND WINDOWS $vcenter $patchgroupArray{$virtualmachine} $virtualmachine\n";
				open (LIST, ">> new-files/$vcenter-$patchgroupArray{$virtualmachine}-windows.TMP");		
				&printLine;
				#print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notesArray{$virtualmachine}\n";
				#print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$virtualmachineArray{$virtualmachine},$uuid,$notes\n";
				close LIST;
				open (FULL, ">> new-files/$vcenter-$patchgroupArray{$virtualmachine}-full.TMP");		
				&printFull;
				#print FULL "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notesArray{$uuid},$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notesArray{$virtualmachine}\n";
				close FULL;
			}
			if ($patchgroupArray{$virtualmachine} eq "") {

 				open (LIST, ">> new-files/$vcenter-problems.TMP");
				&printLine;
                                #print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notesArray{$virtualmachine}\n";
                                close LIST;

				##SPM##print "Something went wrong: $list :$patchgroupArray{$virtualmachine}:\n";	
#				open (LIST, ">> new-files/$vcenter-$patchgroupArray{$virtualmachine}-missingtags.TMP");		
#				print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes\n";
#				#print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$virtualmachineArray{$virtualmachine},$uuid,$notes\n";
#				close LIST;
#				open (FULL, ">> new-files/$vcenter-$patchgroupArray{$virtualmachine}-full.TMP");		
#				print FULL "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes\n";
#				close FULL;
			}
		}else{
			print "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$hostname},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine} EXCLUDED\n";
		}
	}
}

sub printLine {
	print LIST "$virtualmachine,$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notes\n";
	#print LIST "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notes\n";
}

sub printFull {
	print FULL "$virtualmachine,$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notes\n";
	#print FULL "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notes\n";
}

#vdi-eng-0019 , 10.14.46.23 , poweredon , microsoft windows 10 (64-bit) , microsoft windows 10 (64-bit) , eng-vdi ,  , 42223262-fb81-8c6a-c374-cf0482de1a4a
#`grep -i PDV_DBA  /scratch/patchinglist/new-files/*two*|awk -F, '{print $11}'|sort|uniq >  /scratch/patchinglist/new-files/two.clops`;
#`grep -i PDV_DBA  /scratch/patchinglist/new-files/*three*|awk -F, '{print $11}'|sort|uniq >  /scratch/patchinglist/new-files/three.clops`;
#`grep -i pdv /scratch/patchinglist/new-files/clops.three |sort|uniq > /scratch/patchinglist/new-files/clops.three.tmp`;
#`mv /scratch/patchinglist/new-files/clops.three.tmp /scratch/patchinglist/new-files/clops.three`;
#`grep -i pdv /scratch/patchinglist/new-files/clops.two |sort|uniq > /scratch/patchinglist/new-files/clops.two.tmp`;
#`mv /scratch/patchinglist/new-files/clops.two.tmp /scratch/patchinglist/new-files/clops.two`;
#`pwsh /scratch/patchinglist/scripts/get-vc01-newtags.ps1 > /scratch/patchinglist/test-files/missingtags.tmp`;
#`pwsh /scratch/patchinglist/scripts/get-vc02-newtags.ps1 >> /scratch/patchinglist/test-files/missingtags.tmp`;
#`pwsh /scratch/patchinglist/scripts/get-vcs01-newtags.ps1 >> /scratch/patchinglist/test-files/missingtags.tmp`;
#`pwsh /scratch/patchinglist/scripts/get-vcs02-newtags.ps1 >> /scratch/patchinglist/test-files/missingtags.ctmp`;
#`pwsh /scratch/patchinglist/scripts/get-vxvcs01-newtags.ps1 >> /scratch/patchinglist/test-files/missingtags.tmp`;
#`pwsh /scratch/patchinglist/scripts/get-vxvcs02-newtags.ps1 >> /scratch/patchinglist/test-files/missingtags.tmp`;
#`pwsh /scratch/patchinglist/scripts/get-eng-newtags.ps1 >> /scratch/patchinglist/test-files/missingtags.tmp`;
#`/scratch/patchinglist/scripts/newtags.pl missingtags.tmp > /scratch/patchinglist/new-files/missingtags.csv`;
#`/usr/local/bin/makelower.pl /scratch/patchinglist/new-files/missingtags.csv -f`;

