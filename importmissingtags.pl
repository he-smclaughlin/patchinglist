#!/usr/bin/perl
$email = $ENV{'email'};
#/scratch/patchinglist/missingtags.csv
if (!(-e "/var/lib/jenkins/workspace/inputfiletest/jen-inputfile")) { 
	`echo missing tags | mail -s "missing tags" -a /scratch/patchinglist/missingtags.csv -r smclaughlin\@healthedge.com $email`;
	exit;
} 	
`/usr/local/bin/makelower.pl /scratch/patchinglist/new-files/missingtags.csv -f`;
@oldtags = `cat /scratch/patchinglist/new-files/missingtags.csv`;
foreach $oldtag (@oldtags) {
($virtualmachine,$server,$patchinggroup,$owner,$customer,$customerstatus,$application,$timezone,$os,$datacenter)=split(",", $oldtag);
$datacenter = "he1";
	$virtualmachineArray{$virtualmachine} = $virtualmachine;
	$oldserverArray{$virtualmachine} = $server;
	$oldpatchgroupArray{$virtualmachine} = $patchinggroup;
	$oldownerArray{$virtualmachine} = $owner;
	$oldcustomerArray{$virtualmachine} = $customer;
	$oldcustomerstatusArray{$virtualmachine} = $customerstatus;
	$oldapplicationArray{$virtualmachine} = $application;
	$oldtimezoneArray{$virtualmachine}= $timezone;
	$oldosArray{$virtualmachine} = $os;
	$olddatacenterArray{$virtualmachine} = $datacenter;
}
`/usr/local/bin/makelower.pl /var/lib/jenkins/workspace/inputfiletest/jen-inputfile -f`;
@newtags = `cat /var/lib/jenkins/workspace/inputfiletest/jen-inputfile`;
foreach $newtag (@newtags) {
	($virtualmachine,$server,$patchinggroup,$owner,$customer,$customerstatus,$application,$timezone,$os,$datacenter)=split(",", $newtag);
$datacenter = "he1";
#print "$virtualmachine $oldownerArray{$virtualmachine} $owner\n";
if ($oldownerArray{$virtualmachine} ne $owner ) {
#	print "$virtualmachine:$server:$oldownerArray{$virtualmachine}:$owner\n";
}
#if ($oldpatchgroupArray{$server} ne $patchinggroup ) {
#	print "$server:$oldpatchgroupArray{$server}:$patchinggroup\n";
#}
if ($oldcustomerArray{$server} ne $customer ) {
#	print "$virtualmachine:$server:$oldpatchgroupArray{$virtualmachine}:$patchinggroup\n";
}
#if ($oldcustomerstatusArray{$server} ne $customerstatus ) {
#	print "$server:$oldcustomerstatusArray{$server}:$customerstatus\n";
#}
#if ($oldapplicationArray{$server} ne $application ) {
#	print "$server:$oldapplicationArray{$server}:$application\n";
#}
#if ($oldtimezoneArray{$server} ne $timezone ) {
#	print "$server:$ooldtimezoneArray{$server}:$timezone\n";
#}
#if ($oldosArray{$server} ne $os ) {
#	print "$server:$oldosarray{$server}:$os\n";
#}
#if ($olddatacenterArray{$server} ne $datacenter ) {
#	print "$server:$oldosarray{$server}:$os\n";
#}
	if (defined $oldserverArray{$virtualmachine}) {
		if ($oldpatchgroupArray{$virtualmachine} ne $patchinggroup || $oldownerArray{$virtualmachine} ne $owner || $oldcustomerstatusArray{$virtualmachine} ne $customerstatus || $oldapplicationArray{$virtualmachine} ne $application || $oldtimezoneArray{$virtualmachine} ne $timezone || $oldosArray{$virtualmachine} ne $os || $oldcustomerArray{$virtualmachine} ne $customer) {
			print "$virtualmachine,$server,$patchinggroup,$owner,$customer,$customerstatus,$application,$timezone,$os,$datacenter DIFF\n";
		}else{
			print "$server,$patchinggroup,$owner,$customer,$customerstatus,$application,$timezone,$os SAME\n";
		}
	}
}
#`rm -f /var/lib/jenkins/workspace/inputfiletest/jen-inputfile`;
