#!/usr/bin/perl
$home = "/home/a-smclaughlin/git/test/patchinglist/";
$wwwhome = "/var/www/html/patchlist/";
$env = $ARGV[0];
$patchgroup = $ARGV[1];
`mkdir $home/temp-files 2> /dev/null`;
`mkdir $home/files 2> /dev/null`;
if ($env eq "") {
	print "./process-files.pl VC\n";
	exit;
}
#print "$home/files/$env-ora\n";
open (DCS, "> $home/files/$env-dcs");
open (ORA, "> $home/files/$env-ora");
open (LINUX, "> $home/files/$env-linux");
open (WINDOWS, "> $home/files/$env-windows");
open (FULL, "> $home/files/$env-full");
open (MISSING, "> $home/files/$env-missing");

open (DCSHOSTS, "> $wwwhome/$env-dcs.hosts");
open (ORAHOSTS, "> $wwwhome/$env-ora.hosts");
open (LINUXHOSTS, "> $wwwhome/$env-linux.hosts");
open (WINDOWSHOSTS, "> $wwwhome/$env-windows.hosts");
open (FULLHOSTS, "> $wwwhome/$env-full.hosts");
open (MISSINGHOSTS, "> $wwwhome/$env-missing.hosts");

print DCSHOSTS "[servers]\n";
print ORAHOSTS "[servers]\n";
print LINUXHOSTS "[servers]\n";
print WINDOWSHOSTS "[servers]\n";
print FULLHOSTS "[servers]\n";

#open (OS, "$home/ip-os");
#while (<OS>) {
#        chomp $_;
#        ($ip2,$os)=split(":", $_);
#        $osArray{$ip2} = $os;
#print "OS $ip2 $osArray{$ip2} $os\n";
#}

open (SYS, "ip-systeminfo");
while (<SYS>) {
        chomp $_;
        ($ip2,$kernel,$uptime,$sssd,$updates,$dupes,$os)=split(":", $_);
print "NONSPLIT $_\n";
	$kernelArray{$ip2} = $kernel;
#print "KER $kernelArray{$ip2}\n";
	$uptimeArray{$ip2} = $uptime;		
	$sssdArray{$ip2} = $sssd;		
	$updatesArray{$ip2} = $update;		
	$dupesArray{$ip2} = $dupes;		
	$osArray{$ip2} = $os;
print "OS:$ip2:$osArray{$ip2}\n";
}
# $env
`makelower.pl $home/temp-files/$env.list -f`;
`makelower.pl $home/temp-files/$env.tags -f`;
#`makelower.pl $home/temp-files/$env.notes -f`;
open (TAGS, "$home/temp-files/$env.tags");
while (<TAGS>) {
        chomp $_;
#	if ($_ =~ /$patchgroup/) {
	        if ($_ =~ /^tag/) {
	                ($blah,$tag)=split("\/", $_);
	#               print "$tag:";
	        }
	        if ($_ =~ /^entity/) {
	                ($blah,$entity)=split(": ", $_);
	#               print "$entity $tag\n";
	                $entityArray{$entity} = $tag;
	        }
#	}
}

@excludeos = `cat excludes-OS`;
foreach $excludeos (@excludeos) {
	chomp $excludeos;
	$excludeosArray{$excludeos} = $excludeos;
}

open (NOTES, "$home/temp-files/$env.notes");
while (<NOTES>) {
#rp.phc-ora-pr001.copy.shadow ,  , /home/ansible/Apps/hebb/scoughlin/PDV_DBA/ENV/Paramount/prod/generated-payor-properties/hosts
#hpp-wl-pr01,172.29.10.29 fe80::250:56ff:fe82:b00e,/home/ansible/apps/hebb/scoughlin/pdv_dba/env/hpp/prod/generated-payor-properties/hosts
	chomp $_;
	$_ =~ s/ , /,/g;
	($hostname,$ip,$note)=split(",", $_); 
	if ($ip =~ / /) {
		($ip)=split(" ", $ip);
	}
	$noteArray{$ip} = $note;
}

$print = "yes";
@env = `cat $home/temp-files/$env.list|grep -v ^rp|sed -e "s/ , /,/g"|grep  poweredon`;
foreach $env2 (@env) {
	chomp $env2;
	($hostname,$ip,$power,$os1,$os2,$network)=split(",", $env2);	
if ($hostname =~ /he1-dplora/) {
	print "$hostname $ip\n";
}
$os1 =~ s/\(//g;
$os1 =~ s/\)//g;
	if ($network =~ /\\|/) {
		($network)=split("\\|", $network);
	}
	($ip3)=split(" ", $ip);
#	#print "$hostname,$ip3,$power,$os1,$os2,$network\n";
#if ($kernelArray{$ip3} ne "") {
#print ":$ip3:$kernelArray{$ip3}:$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3}\n";
#}
	chomp $power;
	if (defined $excludeosArray{$os1} || defined $excludeosArray{$os2}) {
	}else{
		&checkpattern;
	}
}
sub checkpattern {
	foreach $excludepattern (@excludepatterns) {
#print "$hostname $excludepattern\n";
		chomp $excludepattern;
		if ($hostname =~ /$excludepattern/) {
			$print = "no";
			$reason = $excludepattern;
		}	
	}
print "NOT $noteArray{$ip3}\n";
	$line = "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$noteArray{$ip3},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3},$osArray{$ip3}";
        print "LINE $line\n";
        if ($entityArray{$hostname} eq "") {
        	print MISSING "$line\n";
        	#print MISSING "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
        	print MISSINGHOSTS "$ip3\n";
        }
	if ($print eq "yes" && $entityArray{$hostname} ne "" && $entityArray{$hostname} eq $patchgroup) {
		#print "$print $entityArray{$hostname} $patchgroup\n";
		if ($hostname =~ /\-dc-/) {
			if ($entityArray{$hostname} eq "") {
				print MISSING "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
				print MISSINGHOSTS "$ip3\n";
			}
			print FULL "$line\n";
			#print FULL "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
			print FULLHOSTS "$ip3\n";
		}elsif ($hostname =~ /\-ora-/) {
print "OHOSTNAME $hostname $os1 $os2\n";
				print ORA "$line\n";
				#print ORA "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
				print ORAHOSTS "$ip3\n";
				print FULL "$line\n";
				#print FULL "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
				print FULLHOSTS "$ip3\n";
		}else{
			if ($os1 =~ /linux/ || $os1 =~ /centos/ || $os2 =~ /linux/ || $os2 =~ /centos/ || $os2 =~ /solaris/) {
print "LHOSTNAME $hostname $os1 $os2\n";
				print LINUX "$line\n";
				#print LINUX "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
				print LINUXHOSTS "$ip3\n";
				print FULL "$line\n";
				#print FULL "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$osArray{$ip3},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
				print FULLHOSTS "$ip3\n";
			}
			if ($os1 =~ /windows/  || $os2 =~ /windows/ ) {
print "WHOSTNAME $hostname $os1 $os2\n";
				print WINDOWS "$line\n";
				#print WINDOWS "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
				print WINDOWSHOSTS "$ip3\n";
				print FULL "$line\n";
				#print FULL "$hostname,$ip3,$power,$os1,$os2,$network,$entityArray{$hostname},$kernelArray{$ip3},$uptimeArray{$ip3},$sssdArray{$ip3},$updatesArray{$ip3},$dupesArray{$ip3},$osArray{$ip3}\n";
				print FULLHOSTS "$ip3\n";
			}
		}
	}else{
#		print "EXCLUDEPAT $reason $hostname,$ip3,$power,$os1,$os2,$network\n";
	}
}


