export DIR="/scratch";
/usr/bin/pwsh $DIR/patchinglist/scripts/get-patchinglist-vc01.ps1 *> $DIR/patchinglist/temp-files/vc01.list &
/usr/bin/pwsh $DIR/patchinglist/scripts/get-patchinglist-vc02.ps1 *> $DIR/patchinglist/temp-files/vc02.list
/usr/bin/pwsh $DIR/patchinglist/scripts/get-vc01-tags.ps1 *> $DIR/patchinglist/temp-files/vc01.tags &
/usr/bin/pwsh $DIR/patchinglist/scripts/get-vc02-tags.ps1 *> $DIR/patchinglist/temp-files/vc02.tags
/usr/bin/pwsh $DIR/patchinglist/scripts/get-vc01-notes.ps1 *> $DIR/patchinglist/temp-files/vc01.notes &
/usr/bin/pwsh $DIR/patchinglist/scripts/get-vc02-notes.ps1 *> $DIR/patchinglist/temp-files/vc02.notes
