#!/usr/bin/perl
# get info from lists files
# list of vcenters
`rm -f new-files/NEW-full.VCMDB`;
$vcenters = "vc01 vc02 vcs01 vcs02 vxvcs01 vxvcs02 eng";
(@vcenters)=split(" ", $vcenters);
foreach $vcenter (@vcenters) {
	chomp $vcenter;
	`rm -f new-files/$vcenter-*.VCMDB`;
	&load_tags;
	&load_lists;
}

sub load_tags {
#`/usr/local/bin/makelower.pl /scratch/patchinglist/test-files/$vcenter.tags.tmp -f`;
open (TAGS, "/scratch/patchinglist/test-files/$vcenter.tags.tmp");
while (<TAGS>) {
        chomp $_;
	if ($_ =~ /VirtualMachine/) {
		$_ =~ s/[=\/]/ /g;
		($blah,$virtualmachine1,$virtualmachine2)=split(" ", $_);
		if ($virtualmachine1 eq "VirtualMachine") { $virtualmachine = $virtualmachine2;}	
		if ($virtualmachine1 ne "VirtualMachine") { $virtualmachine = $virtualmachine1;}	
		if ($_ =~ / VirtualMachine-vm-1267/) {print "VIRT $_ $virtualmachine\n";}
	}
	if ($_ =~ /^Tag/ && $_ =~ /Patching Group/) {
		($blah,$tag)=split(": ", $_);
		($blah,$tag)=split("\/", $_);
		$patchgroupArray{$virtualmachine} = $tag;	
if ($hostname =~ /heeng-ashitole/) { print "PATCHGROUP  $virtualmachine $patchgroupArray{$virtualmachine} $tag $_\n";}
	}
	if ($_ =~ /^Tag/ && $_ =~ /Internal Owner/) {
       		($blah,$owner)=split("\/", $_);
	       	$ownerArray{$virtualmachine}= $owner;
       	}
	
       	if ($_ =~ /^Tag/ && $_ =~ /Customer/ && $_ !~ /Customer Status/) {
	       	($blah,$customer)=split("\/", $_);
	       	$customerArray{$virtualmachine}= $customer;
       	}
	if ($_ =~ /^Tag/ && $_ =~ /Application/) {
	       	($blah,$application)=split("\/", $_);
	       	$applicationArray{$virtualmachine}= $application;
       	}
	
       	if ($_ =~ /^Tag/ && $_ =~ /Time Zone/) {
	       	($blah,$timezone)=split("\/", $_);
	       	$timezoneArray{$virtualmachine}= $timezone;
       	}
        if ($_ =~ /^Tag/ && $_ =~ /OS/) {
	       	($blah,$os)=split("\/", $_);
	     $osArray{$virtualmachine}= $os;
	}
	if ($_ =~ /^Tag/ && $_ =~ /Customer Status/) {
		($blah,$customerstatus)=split("\/", $_);
		$customerstatusArray{$virtualmachine}= $customerstatus;
	}
	if ($_ =~ /^Tag/ && $_ =~ /Environment/) {
		($blah,$environment)=split("\/", $_);
		$environmentArray{$virtualmachine}= $environment;
	}
	if ($_ =~ /^Entity/) {
		($blah,$blah,$hostname)=split(" ", $_);
	}
}
}



sub load_lists {
#	`/usr/local/bin/makelower.pl /scratch/patchinglist/test-files/$vcenter.list.tmp -f `;
	@list = `grep -i Powered /scratch/patchinglist/test-files/$vcenter.list.tmp |grep -v ^rp`; #|sed -e "s/ , /,/g"`;
	foreach $list (@list) {
		chomp $list;
#		$list =~ tr/[A-Z]/[a-z]/;
        	($hostname,$ip,$power,$os1,$os2,$network,$blah,$uuid,$virtualmachine,$notes)=split(" , ", $list);
		$os1 =~ s/,//g;
		$os2 =~ s/,//g;
if ($hostname =~ /heeng-ashitole/) { print "LIST $list\n";}
		if (!(defined $uuidArray{$uuid})) {
			$uuidArray{$uuid} = $uuid;
			$os1 =~ s/[\(\)]//g;
			$os2 =~ s/[\(\)]//g;
			if ($ip =~ / / && $ip =~ /^172/) {
				($ip)=split(" ", $ip);
			#	print "$ip\n";
			}elsif ($ip =~ / / && $ip =~ / fe80/) {
				($ip)=split(" ", $ip);
				#print "$ip\n";
			}elsif ($ip =~ / / && $ip =~ /^2002/) {
				($blah,$blah,$ip)=split(" ", $ip);
				#print "$ip\n";
			}elsif ($ip =~ / / && $ip =~ /^10/ && $ip =~ / 172/) {
				($ip)=split(" ", $ip);
			}
	#		$virtualmachine =~ tr/[A-Z]/[a-z]/;
			if ($hostname !~ /rp./ && $hostname !~ /shadow/) {
if ($hostname =~ /heeng-ashitole/) { print "PRINT $list\n";
	print "$patchgroupArray{$virtualmachine} $virtualmachine,$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notes\n";
}
				open (FULL, ">> new-files/NEW-full.VCMDB");		
				&printFull2;
			}
		}else{
#print "DUPE $list\n";
		}
	}
}


#ciampa-linux2 , 422c79c5-f8ba-9bd5-2d00-c1148ee4ba6f , VirtualMachine-vm-679330i#

sub printFull2 {
	if ($ip eq "192.168.122.1") { $ip = "";}
	print FULL "$virtualmachine,$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notes\n";
	#print FULL "$vcenter,$hostname,$ip,$power,$os1,$os2,$network,$patchgroupArray{$virtualmachine},$uuid,$notes,$ownerArray{$virtualmachine},$customerArray{$virtualmachine},$applicationArray{$virtualmachine},$timezoneArray{$virtualmachine},$osArray{$virtualmachine},$customerstatusArray{$virtualmachine},$environmentArray{$virtualmachine},$notes\n";
}
