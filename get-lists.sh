cd /home/a-smclaughlin/git/test/patchinglist/

mkdir files 2> /dev/null
date > files/time
echo "starting eng.list"
mkdir temp-files 2> /dev/null
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-patchinglist-eng.ps1  *> /home/a-smclaughlin/git/test/patchinglist/temp-files/eng.list 
echo "starting vcs-01.list"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-patchinglist-vcs1.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vcs01.list 
echo "starting vcs-02.list"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-patchinglist-vcs2.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vcs02.list 
echo "starting vxvcs01.list"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-patchinglist-vx1.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vxvcs01.list 
echo "starting vxvcs02.list"
/usr/bin/pwsh /home/a-smclaughlin/git/test/patchinglist/get-patchinglist-vx2.ps1 *> /home/a-smclaughlin/git/test/patchinglist/temp-files/vxvcs02.list 
