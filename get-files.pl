date > files/time
echo "starting eng.list"
mkdir temp-files 2> /dev/null
/usr/bin/pwsh get-patchinglist-eng.ps1 *> temp-files/eng.list
echo "starting vcs-01.list"
/usr/bin/pwsh get-patchinglist-vcs1.ps1 *> temp-files/vcs01.list
echo "starting vcs-02.list"
/usr/bin/pwsh get-patchinglist-vcs2.ps1 *> temp-files/vcs02.list
echo "starting vxvcs01.list"
/usr/bin/pwsh get-patchinglist-vx1.ps1 *> temp-files/vxvcs01.list
echo "starting vxvcs02.list"
/usr/bin/pwsh get-patchinglist-vx2.ps1 *> temp-files/vxvcs02.list

echo "starting eng.tags"
/usr/bin/pwsh get-engtags.ps1 *> temp-files/eng.tags
echo "starting vcs01.tags"
/usr/bin/pwsh get-he1leg-tags.ps1 *> temp-files/vcs01.tags
echo "starting vxvcs01.tags"
/usr/bin/pwsh get-he1vx-tags.ps1 *> temp-files/vxvcs01.tags
echo "starting vcs02.tags"
/usr/bin/pwsh get-he2leg-tags.ps1 *> temp-files/vcs02.tags
echo "starting vxvcs02.tags"
/usr/bin/pwsh get-he2vx-tags.ps1 *> temp-files/vxvcs02.tags
date >> files/time
