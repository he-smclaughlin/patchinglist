$username = "a-smclaughlin"
$encrypted = Get-Content /scratch/tagging/encrypted_password.txt | ConvertTo-SecureString
$credential = New-Object System.Management.Automation.PsCredential($username, $encrypted)
Connect-VIServer -Server $args -Protocol https -Credential $credential
$FileContent = Get-content /scratch/patchinglist/new-files/$args
foreach($VM in $FileContent) {
	$array = $VM.Split(" ")
	$String1 = $array[0]
	$String2 = $array[1]
	#write-host "HOST $String1 CUST $String2"
	Get-VM $array[0]|New-TagAssignment -Tag $String2
}
